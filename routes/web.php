<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middlewere'=>'auth'], function(){
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/changepwd','UserController@changepwd');
  Route::post('/updatepwd','UserController@updatepwd');
});



Route::group(['prefix'=>'user'], function(){
    Route::get('/reg','UserController@reg')->name('reg');
    Route::get('/cart',"UserController@cart")->name('cart');
    Route::get('/checkout','UserController@checkout')->name('checkout');
    Route::get('/catalog','UserController@catalog')->name('catalog');
    Route::get('/fashion','UserController@fashions')->name('fashion');
    Route::get('/babytoys','UserController@babytoys')->name('babytoys');
    Route::get('/electronics','UserController@electronics')->name('electronics');
    Route::get('/fitness','UserController@fitness')->name('fitness');
    Route::get('/supermkt','UserController@supermkt')->name('supermkt');
    Route::get('/logout','UserController@logout')->name('logout');
    Route::get('/loggin','UserController@loggin')->name('loggin');
    Route::post('/userreg','UserController@user_reg')->name('user_reg');
    Route::post('/userlogin','UserController@userlogin')->name('userlogin');
    Route::get('/welcome','UserController@welcome')->name('welcome');
    

  });

  Route::group(['prefix'  =>  'admin'], function () {

    Route::get('addstaff', 'UserController@addstaff')->name('addstaff');
    Route::post('/staffreg','UserController@staff_reg')->name('staff_reg');
    Route::get('/viewstaff', 'UserController@viewstaff')->name('viewstaff');
    Route::get('/addgoods', 'UserController@addgoods')->name('addgoods');
    Route::get('/viewgoods', 'UserController@viewgoods')->name('viewgoods');
    Route::get('/calender','UserController@calender')->name('calender');
    Route::get('/invoice','UserController@invoice')->name('invoice');
    Route::get('/mailbox','UserController@mailbox')->name('mailbox');
    Route::get('/pageuser','UserController@pageuser')->name('pageuser');
    Route::post('/update','UserController@update')->name('update');
    Route::post('/submitgd','UserController@submitgd')->name('submitgd');

  });
  route::get('/delete/{id}',[
    'uses'=>'UserController@delete',
    'as'=>'delete',
   ]);
   route::get('/edit/{id}',[
    'uses'=>'UserController@edit',
    'as'=>'edit',
   ]);

