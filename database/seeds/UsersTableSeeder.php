<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();

        User::create([
            'firstname'=>'admin',
            'lastname'=>'ochola',
            'user_type'=>'admin',
            'email'=>'admin@gmail.com',
            'gender'=>'male',
            'city'=>'Nairobi',
            'country'=>'Kenya',
            'password'=>bcrypt('password')
         ]);
         User::create([
            'firstname'=>'staff',
            'lastname'=>'ochola',
            'user_type'=>'staff',
            'email'=>'staff@gmail.com',
            'gender'=>'male',
            'city'=>'Nairobi',
            'country'=>'Kenya',
            'password'=>bcrypt('password')
         ]);
         User::create([
            'firstname'=>'user',
            'lastname'=>'ochola',
            'user_type'=>'user',
            'email'=>'user@gmail.com',
            'gender'=>'male',
            'city'=>'Nairobi',
            'country'=>'Kenya',
            'password'=>bcrypt('password')
         ]);
    }
}
