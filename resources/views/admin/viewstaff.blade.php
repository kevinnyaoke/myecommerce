@extends('user.app')
@section('title') Dashboard @endsection
@section('content')
<section class="section-pagetop bg-dark">
    <div class="container clearfix">
        <h2 class="title-page">Staff Members</h2>
    </div> <!-- container //  -->
</section>
<div class="container">

    <h3>Staff Available</h3><br><br>

    @include('includes.message')
    <div class="table-responsive">
        <table class="table table-hover" id="myTable">
            <thead>
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>User-Type</th>
                    <th></th>


                </tr>
            </thead>
            <tbody id="">
                @foreach($staff as $staff)
                <tr>
                    <td>{{$staff->firstname}}</td>
                    <td>{{$staff->lastname}}</td>
                    <td>{{$staff->email}}</td>
                    <td>{{$staff->gender}}</td>
                    <td>{{$staff->user_type}}</td>
                    <td><a class="btn btn-danger" href="{{route('delete',['id'=>$staff->id])}}">Delete</a></td>
                    <td><a class="btn btn-primary" href="{{route('edit',['id'=>$staff->id])}}">Edit</a></td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection