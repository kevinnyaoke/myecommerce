<header class="section-header">
    <section class="header-main">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="brand-wrap">
                        <img class="logo" src="{{asset('frontend/images/logo-dark.png')}}">
                        <h2 class="logo-text">LOGO </h2>
                    </div>
                    <!-- brand-wrap.// -->
                </div>
                <div class="col-lg-6 col-sm-6">
                    <form action="#" class="search-wrap">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- search-wrap .end// -->
                </div>
                <!-- col.// -->
                <div class="col-lg-3 col-sm-6">
                    <div class="widgets-wrap d-flex justify-content-end">
                    @cannot('isAdmin')
                        <div class="widget-header">
                            <a href="{{route ('cart')}}" class="icontext">
                                <div class="icon-wrap icon-xs bg2 round text-secondary"><i
                                        class="fa fa-shopping-cart"></i></div>
                                <div class="text-wrap">
                                    <small>Basket</small>
                                    <span>3 items</span>
                                </div>
                            </a>
                        </div>
                        @endcannot

                        <!-- @cannot('isStaff')
                        <div class="widget-header">
                            <a href="{{route ('cart')}}" class="icontext">
                                <div class="icon-wrap icon-xs bg2 round text-secondary"><i
                                        class="fa fa-shopping-cart"></i></div>
                                <div class="text-wrap">
                                    <small>Basket</small>
                                    <span>3 items</span>
                                </div>
                            </a>
                        </div>
                        @endcannot -->
                        <!-- widget .// -->
                        <div class="widget-header dropdown">
                            <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                                <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                                <div class="text-wrap">
                                    <small>Hello</small>
                                    <span> <i class="fa fa-caret-down"></i></span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <hr class="dropdown-divider">

                                <a class="dropdown-item" href="{{ route('logout') }}"><i
                                        class="fa fa-sign-out fa-lg"></i>
                                    Logout</a>
                                <a class="dropdown-item" href="{{route('loggin')}}">Have account? Sign up</a>
                                <a class="dropdown-item" href="{{url ('/changepwd')}}">Change password?</a>
                            </div>
                            <!--  dropdown-menu .// -->
                        </div>
                        <!-- widget  dropdown.// -->
                    </div>
                    <!-- widgets-wrap.// -->
                </div>
                <!-- col.// -->
            </div>
            <!-- row.// -->
        </div>
        <!-- container.// -->
    </section>
    <!-- header-main .// -->

    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
        <div class="container">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"
                aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav">
                @can('isAdmin')
                <li class="nav-item">
                        <a class="nav-link pl-0" href=""> <strong>Admin Dashboard </strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="{{route ('home')}}"> <strong>Home</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('mailbox')}}">Mailbox</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('invoice')}}">Invoice</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('calender')}}">Calender</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown07"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Staff</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown07">
                            <a class="dropdown-item" href="{{route('addstaff')}}">Add Staff</a>
                            <a class="dropdown-item" href="{{route('viewstaff')}}">View Staff</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown07"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Goods</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown07">
                            <a class="dropdown-item" href="{{route('addgoods')}}">New Goods</a>
                            <a class="dropdown-item" href="{{route('viewgoods')}}">View Goods</a>
                        </div>
                    </li>
                    @endcan
                </ul>
            </div>
          

            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav">
                    @can('isStaff')
                    <li class="nav-item">
                        <a class="nav-link pl-0" href=""> <strong>Staff Dashboard </strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="{{route ('home')}}"> <strong>Home</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('mailbox')}}">Mailbox</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('invoice')}}">Invoice</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('calender')}}">Calender</a>
                    </li>

                    @endcan

                </ul>
            </div>



            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav">
                    @can('isUser')
                    <li class="nav-item">
                        <a class="nav-link pl-0" href=""> <strong>User Dashboard</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="{{route ('home')}}"> <strong>Home</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('cart')}}">Cart</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('catalog')}}">Catalog list</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('checkout')}}">Checkout</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown07"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown07">
                            <a class="dropdown-item" href="{{route('fashion')}}">Fashions</a>
                            <a class="dropdown-item" href="{{route('supermkt')}}">Supermarkets</a>
                            <!-- <div class="dropdown-divider"></div> -->
                            <a class="dropdown-item" href="{{route('electronics')}}">Electronics</a>
                            <a class="dropdown-item" href="{{route('babytoys')}}">Baby &amp Toys</a>
                            <a class="dropdown-item" href="{{route('fitness')}}">Fitness & sports</a>
                        </div>
                    </li>
                    @endcan
                </ul>
            </div>



            <!-- collapse .// -->
        </div>
        <!-- container .// -->
    </nav>

</header>