@extends('user.app')
@section('title') Dashboard @endsection
@section('content')

        <section class="section-content bg padding-y">
        <div class="container">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <header class="card-header">
                        <h4 class="card-title mt-2">Change Password</h4>
                        @include('includes.message')
                    </header>
                    <article class="card-body">

                        <form role="form" method="POST" action="{{url ('/updatepwd')}}"> 
                              @csrf
                            <!-- form-row.// -->
                            <div class="form-group">
                                <label>Current password</label>
                                <input class="form-control" name="cpass" type="password" required>
                            </div>
                            <div class="form-group">
                                <label>New password</label>
                                <input class="form-control" name="npass" type="password" required>
                            </div>
                            <div class="form-group">
                                <label>Confirm password</label>
                                <input class="form-control" name="repass" type="password" required>
                            </div>
                            <!-- form-group end.// -->
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block"> Register </button>
                            </div>
                            <!-- form-group// -->
                            <small class="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br> Terms of use and Privacy Policy.</small>
                        </form>
                    </article>
                    <!-- card-body end .// -->
                    <div class="border-top card-body text-center">Have an account? <a href="">Log In</a></div>
                </div>
                <!-- card.// -->
            </div>
        </div>
    </section>
        
        
         <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->
    <!-- ========================= SECTION CONTENT END// ========================= -->
  
    <!-- ========================= FOOTER END // ========================= -->
@endsection
