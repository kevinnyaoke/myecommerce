<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Traits\UploadTrait;
use App\User;
use Auth;
use App\Goods;

class UserController extends Controller
{

    use UploadTrait;

    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session();
        // Auth::logout();
        // session::flush();
        return view('auth.login');

    }

    public function reg()
    {
        return view('user.pages.register');
    }

    public function user_reg(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'user_type' => 'required',
            'email' => 'required|email',
            'gender' => 'required',
            'city' => 'required',
            'country' => 'required',
            'password' => 'required|min:8|confirmed',
          
        ]);

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $user_type = $request->input('user_type');
        $email = $request->input('email');
        $gender = $request->input('gender');
        $city = $request->input('city');
        $country = $request->input('country');
        $password = $request->input('password');

        User::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'user_type' => $user_type,
            'email' => $email,
            'gender' => $gender,
            'city' => $city,
            'country' => $country,
            'password' => Hash::make($password),

        ]);
        return redirect()->route('home');
    

    }

    public function staff_reg(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'user_type' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'city' => 'required',
            'country' => 'required',
            // 'password' => 'required|min:8',
            // 'cpassword'=>'required|min:8',
        ]);

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $user_type = $request->input('user_type');
        $email = $request->input('email');
        $gender = $request->input('gender');
        $city = $request->input('city');
        $country = $request->input('country');

        User::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'user_type' => $user_type,
            'email' => $email,
            'gender' => $gender,
            'city' => $city,
            'country' => $country,
            'password' => Hash::make($email),

        ]);
        return redirect()->back()->with('success','New staff added successfully');
    }
    public function loggin()
    {
        return view('auth.login');
    }

    public function userlogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'password',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
            return redirect()->route('home');
        }
        return redirect()->back()->with('error', 'Wrong email or password');

    }

    public function welcome()
    {
        return view('welcome');
    }

    public function cart()
    {
        return view('user.pages.cart');
    }

    public function catalog()
    {
        return view('user.pages.catalog');
    }

    public function checkout()
    {
        return view('user.pages.checkout');
    }

    public function babytoys()
    {
        return view('user.categories.babytoys');
    }
    public function electronics()
    {
        return view('user.categories.electronics');
    }

    public function fashions()
    {
        return view('user.categories.fashions');
    }
    public function fitness()
    {
        return view('user.categories.fitness');
    }
    public function supermkt()
    {
        return view('user.categories.supermkt');
    }

    public function mailbox()
    {
        return view('staff.mailbox');
    }
    public function invoice()
    {
        return view('staff.invoice');
    }
    public function calender()
    {
        return view('staff.calender');
    }
    public function addgoods()
    {
        return view('admin.addgoods');
    }
    public function viewgoods()
    {
        return view('admin.viewgoods');
    }

    public function addstaff()
    {
        return view('admin.addstaff');
    }

    public function viewstaff()
    {
        $staff=User::all()->where('user_type',"staff");
        return view('admin.viewstaff',compact('staff'));
    }


    public function delete($id){
        $staff=User::find($id);
        $staff->delete();
        return redirect()->back()->with('success','Staff deleted');
    }

    public function edit($id){
        $staff=User::find($id);
        return view('admin.editstaff',compact('staff'));
    }

    public function update(Request $request){

        $request->validate([
            'firstname'=>'required',
            'lastname'=>'required',
            'gender'=>'required',
            'user_type'=>'required',
            'city'=>'required',
            'country'=>'required',
        ]);

        $id=$request->input('id');
        $firstname=$request->input('firstname');
        $lastname=$request->input('lastname');
        $gender=$request->input('gender');
        $user_type=$request->input('user_type');
        $city=$request->input('city');
        $country=$request->input('country');

        $stafff=User::find($id);
      if(!$stafff){
          return redirect()->back()->with('error','cannot update staff');
      }

        $stafff->firstname=$firstname;
        $stafff->lastname=$lastname;
        $stafff->gender=$gender;
        $stafff->user_type=$user_type;
        $stafff->city=$city;
        $stafff->country=$country;
        $stafff->update();
        
        $staff=User::all()->where('user_type','staff');
        return view('admin.viewstaff',compact('staff'))->with('success','staff updated successfully');
    }

    public function changepwd(){
        return view('changepwd');
    }

    public function updatepwd(Request $request){
        $this->validate($request, [
            'cpass' => 'required|min:8',
            'npass' => 'required|min:8',
            'repass' => 'required|min:8',
        ]);
        //Get the current authenticated user password
        $currentpass = auth()->user()->password;
        //Check if passwords match
        if (!Hash::check($request['cpass'], $currentpass)) {
            return redirect()->back()->with('error', 'Sorry the entered current password is wrong.');
        } elseif ($request->input('npass') !== $request->input('repass')) {
    
            return redirect()->back()->with('error', 'Sorry the new passwords don\'t match.');
        }
    //Update password
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
    
        $currentUser->password = Hash::make($request->input('npass'));
        $currentUser->save();
        return redirect()->back()->with('success','Password changed ,You must logout to login in with  new passowrd');
    }

    public function submitgd(Request $request){
        $request->validate([
            'name'=>'required',
            'bprice'=>'required|numeric',
            'sprice'=>'required|numeric',
            'description'=>'required',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif|max:10000'
        ]);

                
        // $goods = User::findOrFail(auth()->user()->id);// Get current user
        // $user->name = $request->input('name');// Set user name
        if ($request->has('photo')) {  // Check if a product image has been uploaded
            $image = $request->file('photo');// Get image file
            $extension=$image -> getClientOriginalExtension();
            $filename = time() . '.' . $extension; 
            $image ->move( 'uploads/images/', $filename);// Define folder path
            $goods->photo= $filename;// Set product image path in database to filePath
        }else{
            return $request;
            $goods->image='';
        }

        // $goods->save(); // Persist user record to database
        $goods=Goods::create($request->all());
        return redirect()->back()->with('success','New product successfully added to store');
    }


}